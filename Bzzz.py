import smartpy as sp


class Bzzz(sp.Contract):
    def __init__(self, admin, found, private_end_date, pre_end_date, limit):
        self.init(paused=False, balances=sp.big_map(), administrator=admin, foundation=found, totalSupply=0, private_end_date=sp.timestamp(private_end_date), pre_end_date=sp.timestamp(pre_end_date), supplyLimit=limit, xtzContribution=0)

    @sp.entry_point
    def transfer(self, params):
        sp.verify((sp.sender == self.data.administrator) |
                  (~self.data.paused &
                   ((params.fromAddr == sp.sender) |
                    (self.data.balances[params.fromAddr].approvals[sp.sender] >= params.amount))))
        sp.verify(params.amount >= 2)
        self.addAddressIfNecessary(params.toAddr)
        sp.verify(self.data.balances[params.fromAddr].balance >= params.amount)
        self.data.balances[params.fromAddr].balance -= params.amount
        divider = sp.nat(20)
        natAmount = sp.as_nat(params.amount)
        divided_balance = natAmount // divider
        rest_balance = natAmount % divider
        self.data.balances[params.toAddr].balance += sp.to_int(divided_balance * 19 + rest_balance) - 1
        self.data.balances[self.data.administrator].balance += sp.to_int(divided_balance) + 1
        sp.if (params.fromAddr != sp.sender) & (self.data.administrator != sp.sender):
            self.data.balances[params.fromAddr].approvals[params.toAddr] -= params.amount

    @sp.entry_point
    def approve(self, params):
        sp.verify((sp.sender == self.data.administrator) |
                  (~self.data.paused & (params.fromAddr == sp.sender)))
        sp.verify(self.data.balances[params.fromAddr].approvals.get(params.toAddr, 0) == 0)
        self.data.balances[params.fromAddr].approvals[params.toAddr] = params.amount

    @sp.entry_point
    def setPause(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.data.paused = params

    @sp.entry_point
    def setAdministrator(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.addAddressIfNecessary(self.data.administrator)
        self.data.administrator = params

    def mintInternal(self, address, amount):
        sp.verify(self.data.totalSupply + amount <= self.data.supplyLimit)
        self.addAddressIfNecessary(address)
        self.data.balances[address].balance += amount
        self.data.totalSupply += amount

    @sp.entry_point
    def burn(self, params):
        sp.verify(sp.sender == self.data.administrator)
        sp.verify(self.data.balances[params.address].balance >= params.amount)
        self.data.balances[params.address].balance -= params.amount
        self.data.totalSupply -= params.amount
        
    @sp.entry_point
    def updateSupplyLimit(self, params):
        sp.verify(sp.sender == self.data.administrator)
        sp.if (sp.timestamp(1640995201) < sp.now) & (self.data.supplyLimit < 50000000000):
            self.data.supplyLimit += 500000000
            self.mintInternal(self.data.administrator, 250000000)
        sp.if (sp.timestamp(1625097601) < sp.now) & (sp.now <= sp.timestamp(1640995201)):
            self.data.supplyLimit += 4000000000
            self.mintInternal(self.data.administrator, 2000000000)
        sp.if (sp.timestamp(1609459201) < sp.now) & (sp.now <= sp.timestamp(1625097601)):
            self.data.supplyLimit += 4000000000
            self.mintInternal(self.data.administrator, 2000000000)

    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(balance=0, approvals={})

    @sp.entry_point
    def privateSale(self, params):
        sp.verify(sp.now <= self.data.private_end_date)
        natAmount = sp.as_nat(params.amount)
        tezValue = sp.tez(natAmount)
        sp.verify(sp.amount == tezValue)
        self.processSplit(sp.amount)
        sp.if natAmount < 10000:
            self.mintInternal(sp.sender, params.amount * 173)
        sp.if (10000 <= natAmount) & (natAmount < 100000):
            self.mintInternal(sp.sender, params.amount * 188)
        sp.if natAmount >= 100000:
            self.mintInternal(sp.sender, params.amount * 195)
        self.data.xtzContribution += params.amount

    @sp.entry_point
    def preSale(self, params):
        sp.verify((self.data.private_end_date < sp.now) & (sp.now <= self.data.pre_end_date))
        natAmount = sp.as_nat(params.amount)
        tezValue = sp.tez(natAmount)
        sp.verify(sp.amount == tezValue)
        self.processSplit(sp.amount)
        sp.if natAmount < 10000:
            self.mintInternal(sp.sender, params.amount * 165)
        sp.if (10000 <= natAmount) & (natAmount < 100000):
            self.mintInternal(sp.sender, params.amount * 180)
        sp.if natAmount >= 100000:
            self.mintInternal(sp.sender, params.amount * 188)
        self.data.xtzContribution += params.amount

    @sp.entry_point
    def sale(self, params):
        sp.verify(self.data.pre_end_date < sp.now)
        natAmount = sp.as_nat(params.amount)
        tezValue = sp.tez(natAmount)
        sp.verify(sp.amount == tezValue)
        self.processSplit(sp.amount)
        self.mintInternal(sp.sender, params.amount * 150)
        self.data.xtzContribution += params.amount
        
    def processSplit(self, amount):
        sp.send(self.data.foundation, sp.split_tokens(amount, 67, 100))
        sp.send(self.data.administrator, sp.split_tokens(amount, 33, 100))

    @sp.entry_point
    def getBalance(self, params):
        sp.transfer(sp.as_nat(self.data.balances[params.owner].balance), sp.tez(0),
                    sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getTotalSupply(self, params):
        sp.transfer(sp.as_nat(self.data.totalSupply), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getAdministrator(self, params):
        sp.transfer(self.data.administrator, sp.tez(0), sp.contract(sp.TAddress, params.target).open_some())


if "templates" not in __name__:
    @sp.add_test(name="Bzzz")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("Bzzz Contract")
        private_end_date = 1581074816
        pre_end_date = 1581074816
        limit = 15000000000

        admin = sp.address("tz1eRtFtKik3LyDvqVt3csXc64y6nn5BXyps")
        found = sp.address("tz1cbSBCKFVAro6u8AXbQRsv6aeFt9XjKvc5")
        jack = sp.address("tz1aJLzguZuqbf1oH8aSPPiqrjed4H1YRDFi")
        will = sp.address("tz1MGJKeEoJpNZY3rP9V8yHWVrLPSRJvTyU2")

        c1 = Bzzz(admin, found, private_end_date, pre_end_date, limit)

        scenario += c1
        # scenario.h2("Admin mints a few coins to Jack's wallet")
        # scenario += c1.mint(address=jack, amount=12).run(sender=admin)
        # scenario += c1.mint(address=jack, amount=3).run(sender=admin)
        # scenario += c1.mint(address=jack, amount=3).run(sender=admin)

        # scenario.h2("Jack transfers to Will")
        # scenario += c1.transfer(fromAddr=jack, toAddr=will, amount=4).run(sender=jack)
        

        # scenario.h2("Will tries to over-transfer to Jack")
        # scenario += c1.transfer(fromAddr=will, toAddr=jack, amount=5).run(sender=will, valid=False)
        
        # scenario.h2("Admin burns Will token")
        # scenario += c1.burn(address=will, amount=1).run(sender=admin)
        
        # scenario.h2("Jack tries to burn Will token")
        # scenario += c1.burn(address=will, amount=1).run(sender=jack, valid=False)
        
        # scenario.h2("Admin pauses the contract and Jack cannot transfer anymore")
        # scenario += c1.setPause(True).run(sender=admin)
        # scenario += c1.transfer(fromAddr=jack, toAddr=will, amount=4).run(sender=jack, valid=False)
        
        # scenario.h2("Admin transfers while on pause")
        # scenario += c1.transfer(fromAddr=jack, toAddr=will, amount=1).run(sender=admin)
        
        # scenario.h2("Admin unpauses the contract and transferts are allowed")
        # scenario += c1.setPause(False).run(sender=admin)
        # scenario += c1.transfer(fromAddr=jack, toAddr=will, amount=1).run(sender=jack)
        scenario.h3("Set Admin address")
        scenario += c1.setAdministrator(admin).run(sender=admin)

        scenario.h3("privateSaleContract")
        scenario += c1.privateSale(amount=2).run(sender=jack, amount=sp.tez(2))
        scenario += c1.privateSale(amount=10000).run(sender=jack, amount=sp.tez(10000))
        scenario += c1.privateSale(amount=110000).run(sender=will, amount=sp.tez(110000))
        
        scenario.h2("Admin update totalSupply limit")
        scenario += c1.updateSupplyLimit().run(sender=admin)
        
        scenario.h2("Will transfers to Jack")
        scenario += c1.transfer(fromAddr=jack, toAddr=will, amount=346).run(sender=jack)

        # scenario.verify(c1.data.totalSupply == 17)
        # scenario.verify(c1.data.balances[alice].balance == 8)
        # scenario.verify(c1.data.balances[bob].balance == 9)

        # scenario.h2("Crowdsale")
        # scenario += c1.crowdSale(value=1).run(sender=alice)
        # scenario += c1.mint(address = alice, amount = 1200).run(sender = admin)
        # scenario += c1.mint(address = admin, amount = 120).run(sender = admin)